package cmd

import (
	"encoding/json"
	"errors"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

type FactoryApply struct {
	fileName string
}

type FactoryApplyString string

type JsonDataStruct struct {
	metaData string
}

func FactoryApplyGet(fileName string) (*FactoryApply, error) {
	dataFile, err := os.Open(fileName)
	if err != nil {
		log.Fatal("Cannot open file '" + fileName + "'")
		return nil, errors.New("Cannot open file '" + fileName + "'")
	}
	log.Trace("Opened file '" + fileName + "' sucessfull")
	fileExtension := filepath.Ext(fileName)
	log.Trace("Applying extension '" + fileExtension + "'")
	if fileExtension == ".json" {
		log.Trace("Parsing '" + fileName + "'")
		jsonParser := json.NewDecoder(dataFile)
		var jsonData JsonDataStruct
		if err = jsonParser.Decode(&jsonData); err != nil {
			return nil, errors.New("Cannot parse file '" + fileName + "'")
		}
		log.Trace("File '" + fileName + "' parsed")

		return nil, errors.New("File type '" + fileExtension + "' not supported")
	} else {
		return nil, errors.New("File type '" + fileExtension + "' not supported")
	}
}
