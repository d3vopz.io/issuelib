# issue

## file structure

| Title | Description | Kubernetes  | d3vopz
| :---  | :---- | :--- | :--- |
| User | Top level group | `kubernetes` https://github.com/kubernetes  | `d3vopz.io` https://gitlab.com/d3vopzgolang |
| Repository | Code repository |  `kubernetes` https://github.com/kubernetes/kubernetes | `issue` https://gitlab.com/d3vopzgolang/issue |
| Path | `main()` function | `kubernetes/cmd/kubectl/kubectl.go` | `issue/cmd/issuectl/issuectl.go` | 
| Path | module declaration | `kubernetes/go.mod` | `issue/go.mod` | 
| Path | package declaration | `kubernetes/pkg/kubectl/go.mod` | `issue/pkg/cmd/doc.go` | 
| Path | package implementation | `kubernetes/pkg/kubectl/go.mod` | `issue/pkg/cmd/apply.go` | 
